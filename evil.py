# -*- coding: utf-8 -*-


def ccall(func, pass_name=False):
    """
    def arguments(): pass
    res = func(False, None, 3, key="word", arguments=arguments)``
    # becomes
    class res(ccall(func), False, None, 3):
        key = "word"
        def arguments(): pass
    """

    class metaclass(type):
        def __new__(cls, name, a, d):
            d.pop("__module__", 0)
            if pass_name:
                d["__name__"] = name
            return func(*a[1:], **d)

        def __call__(*a, **d):
            return func(*a, **d)

    return type.__new__(metaclass, func.__name__, (), {})


def pin(loc, *skip):
    """
    def __init__(self, will, *have, **all):
        def the(): pass
        variables = 1
        del but
        pin(locals())
        these = 2
    """
    loc = dict(loc)
    loc.pop("self").__dict__.update(
        {
            k: v
            for k, v in loc.items()
            if not any(
                s is k or s is True and k.startswith("_") or s is v and isinstance(s, object)
                for s in skip
            )
        }
    )


def rebase(obj, want, dont=(), name=None, ns=None):
    if isinstance(obj, type):
        cls = obj
        obj = None
    else:
        assert obj is not None
        cls = type(obj)

    if not isinstance(want, (list, tuple)):
        want = (want,)
    if not isinstance(dont, (list, tuple)):
        dont = (dont,)
    assert want or dont

    want = tuple(w for w in want if w not in cls.__bases__)

    if name is None:
        name = cls.__name__
        for d in dont:
            name = name.replace(d.__name__, "", 1)
        for w in want:
            if w.__name__ not in name:
                name += w.__name__

    bases = []
    for base in cls.__bases__:
        if issubclass(base, dont):
            bases.extend(want)
            want = ()
        else:
            bases.append(base)
    if want:
        bases.extend(want)
    bases = tuple(bases)

    if cls.__bases__ != bases:
        if ns is None:
            ns = dict(cls.__dict__)
            ns.pop("__module__", 1)
        if obj is None:
            cls = type(name, bases, ns)
        else:
            obj.__class__ = type(name, bases, ns)

    return cls if obj is None else obj
