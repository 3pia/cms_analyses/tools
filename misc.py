from tqdm import tqdm


class atq(tqdm):
    auto = None

    def __init__(self, *args, **kwargs):
        if "auto" in kwargs:
            self.auto = kwargs.pop("auto")
        kill1 = kwargs.pop("kill1", True)
        super(atq, self).__init__(*args, **kwargs)
        if kill1 and len(self) <= 1:
            self.close()

    def __iter__(self):
        for obj in super(atq, self).__iter__():
            if self.auto:
                if callable(self.auto):
                    res = self.auto(obj)
                elif isinstance(obj, tuple):
                    res = self.auto.format(*obj)
                else:
                    res = self.auto.format(obj)
                if isinstance(res, dict):
                    _ = res.pop("_", None)
                    self.set_postfix(res)
                    res = _
                if res:
                    self.set_description(res)
            yield obj
