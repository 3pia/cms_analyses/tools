import numpy as np

from .data import DSS


def nan_to_zero(x):
    return x.map(lambda y: np.nan_to_num(y, nan=0, posinf=0, neginf=0))


def abs(x):
    return np.absolute(x) * np.sum(x) / np.sum(np.absolute(x))


def cut(x):
    return np.where(x < 0, 0, x)


def norm(x):
    return x / np.mean(x)


def mask(x, mask):
    return x.map(lambda y: y[mask])


def splitting_function(identifier, n, test=0, valid=0):
    assert test != valid

    def func(x):
        test_mask = (x[identifier] % n == test).flatten()
        valid_mask = (x[identifier] % n == valid).flatten()
        train_mask = ~(test_mask + valid_mask)
        out = {
            "test": mask(x, test_mask.flatten()),
            "valid": mask(x, valid_mask.flatten()),
            "train": mask(x, train_mask.flatten()),
        }
        return out

    return func


def splitting(split, n_splits, identifier):
    if split == -1:
        return lambda x: {
            "valid": x,
            "train": x,
        }
    else:
        n = n_splits
        assert split < n

        test = split
        valid = (test + 1) % n
        func = splitting_function(identifier, n, test=test, valid=valid)
        return func


def get_prepare_train_valid_test(n_splits, identifier):
    def prepare_train_valid_test(x):
        """Prepares DSS object to be split in train valid and test according to event number.
        In the way we do it here (duplicating data), we can keep memmaps for the training.
        Does the task in 2 steps:
            1. duplicates data ([012]->[01201])
            2. saves correct chunk starts and stops such that e.g. for n_splits=3:
                split 0: test=[0], valid=[1], train=[2]
                split 1: test=[1], valid=[2], train=[0]
                split 2: test=[2], valid=[0], train=[1]
        To be used in conjunction with apply_train_valid_test.
        Args:
            x (tools.data.DSS): The input DSS object.

        Returns:
            tools.data.DSS: The output/prepared DSS object.
        """
        masks = ((x[identifier] % n_splits == i).flatten() for i in range(n_splits))
        chunks = [mask(x, _mask) for _mask in masks]
        chunks = chunks + chunks[:-1]

        lenghts = [chunk.blen for chunk in chunks]
        stops = np.cumsum(lenghts).tolist()
        starts = [0] + stops[:-1]

        train, valid, test = [], [], []
        for i in range(n_splits):
            test.append((starts[i], stops[i]))
            valid.append((starts[i + 1], stops[i + 1]))
            train.append((starts[i + 2], stops[i + (n_splits - 1)]))

        data = DSS.concatenate(*chunks)

        data["test"] = test
        data["valid"] = valid
        data["train"] = train

        return data

    return prepare_train_valid_test


def get_apply_train_valid_test(split):
    def function(x):
        if split == -1:
            train_start = valid_start = test_start = x["test"][0][0]
            train_stop = valid_stop = test_stop = x["train"][0][1]
        else:
            train_start, train_stop = x["train"][split][0], x["train"][split][1]
            valid_start, valid_stop = x["valid"][split][0], x["valid"][split][1]
            test_start, test_stop = x["test"][split][0], x["test"][split][1]
        return {
            "train": x.map(lambda y: y[train_start:train_stop]),
            "valid": x.map(lambda y: y[valid_start:valid_stop]),
            "test": x.map(lambda y: y[test_start:test_stop]),
        }

    return function
